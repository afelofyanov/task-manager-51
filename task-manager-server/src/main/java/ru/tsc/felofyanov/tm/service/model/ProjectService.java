package ru.tsc.felofyanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.repository.model.IProjectRepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.model.IProjectService;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.repository.model.ProjectRepository;

import javax.persistence.EntityManager;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }
}
