package ru.tsc.felofyanov.tm.api.repository.model;


import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    User removeByLogin(@Nullable String login);
}
