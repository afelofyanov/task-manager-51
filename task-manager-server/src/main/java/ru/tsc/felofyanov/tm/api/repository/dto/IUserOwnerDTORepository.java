package ru.tsc.felofyanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.AbstractWbsDTO;

import java.util.List;

public interface IUserOwnerDTORepository<M extends AbstractWbsDTO> extends IRepositoryDTO<M> {

    @Nullable
    M create(@Nullable String userId, @NotNull String name);

    @Nullable
    M create(@Nullable String userId, @NotNull String name, @NotNull String description);

    @NotNull
    List<M> findAllByUserId(@Nullable String userId);

    void clearByUserId(@NotNull String userId);

    boolean existsByIdUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndexByUserId(@Nullable String userId, @Nullable Integer index);

    M remove(@Nullable String userId, @Nullable M model);

    M removeByIdByUserId(@Nullable String userId, @Nullable String id);

    M removeByIndexByUserId(@Nullable String userId, @Nullable Integer index);

    long countByUserId(@Nullable String userId);
}
