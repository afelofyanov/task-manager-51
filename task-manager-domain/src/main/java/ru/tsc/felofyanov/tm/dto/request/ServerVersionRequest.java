package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ServerVersionRequest extends AbstractRequest {
}
