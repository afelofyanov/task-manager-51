package ru.tsc.felofyanov.tm.enumerated;

public enum EntityOperationType {

    POST_LOAD,
    PRE_PERSIST,
    POST_PERSIST,
    PRE_UPDATE,
    POST_UPDATE,
    PRE_REMOVE,
    POST_REMOVE
}
