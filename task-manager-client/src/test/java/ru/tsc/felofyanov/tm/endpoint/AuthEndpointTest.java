package ru.tsc.felofyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;
import ru.tsc.felofyanov.tm.dto.request.UserLoginRequest;
import ru.tsc.felofyanov.tm.dto.request.UserLogoutRequest;
import ru.tsc.felofyanov.tm.dto.request.UserProfileRequest;
import ru.tsc.felofyanov.tm.dto.response.UserLoginResponse;
import ru.tsc.felofyanov.tm.dto.response.UserLogoutResponse;
import ru.tsc.felofyanov.tm.dto.response.UserProfileResponse;

public class AuthEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Test
    public void login() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("123", "321")));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("", "")));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(null, null)));

        @Nullable final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test", "test"));
        Assert.assertTrue(response.getSuccess());
        Assert.assertNotNull(response);
        @Nullable final String token = response.getToken();
        Assert.assertNotNull(token);
    }

    @Test
    public void logout() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest()));
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest(null)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest("tester")));

        @Nullable UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        Assert.assertNotNull(loginResponse);
        Assert.assertTrue(loginResponse.getSuccess());

        @NotNull final UserLogoutResponse response = authEndpoint.logout(new UserLogoutRequest(loginResponse.getToken()));
        Assert.assertNotNull(response);
    }

    @Test
    public void profile() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest()));
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest(null)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest("tester")));

        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        Assert.assertNotNull(loginResponse);
        Assert.assertTrue(loginResponse.getSuccess());

        @Nullable final UserProfileResponse response = authEndpoint.profile(new UserProfileRequest(loginResponse.getToken()));
        Assert.assertNotNull(response);

        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user);
    }
}
